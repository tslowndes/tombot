import time
from time import sleep
import board
import busio
import math
from math import atan2, atan
import adafruit_fxos8700
from motor_class import motor_control_class

class adafruit_imu():

	def calibrate(self):
		print('Calibrating IMU')
		xs = []
		ys = []

		motor_control = motor_control_class()

		for i in range(100):

			motor_control.turn_right(25)

			mag_x, mag_y, mag_z = self.sensor.magnetometer
			xs.append(mag_x)
			ys.append(mag_y)

			sleep(.5)
		print()
		print('Max x = {}'.format(max(xs)))
		print('@ max x y = {}'.format(ys[xs.index(max(xs))]))

		print('x offset')
		print('Max y = {}'.format(max(ys)))
		print('@ max y x = {}'.format(xs[ys.index(max(ys))]))
		motor_control.stop()
		

	def get_heading(self):
    	
		mx, my, mz = 0, 0, 0

		for i in range(200):
        	#accel_x, accel_y, accel_z = self.sensor.accelerometer
			mag_x, mag_y, mag_z = self.sensor.magnetometer

			mx += mag_x
			my += mag_y
			mz += mag_z

		mag_x = mx / 200.0
		mag_y = my / 200.0
		mag_z = mz / 200.0

		mag_x = mag_x - 15.7
		mag_y = mag_y - 8.3
        
		heading = abs((180/math.pi) * atan(mag_y/mag_x))

		if mag_y > 0 and mag_x > 0:
			heading = 270 + heading

		elif mag_y < 0 and mag_x > 0:
			heading = 270 - heading

		elif mag_y < 0 and mag_x < 0:
			heading = 90 + heading

		elif mag_y > 0 and mag_x < 0:
			heading = 90 - heading
	    
		self.heading = round(heading)

		return self.heading

	def get_magnetometer(self):
		return self.sensor.magnetometer

	def __init__(self, calibrate = 0):
		i2c = busio.I2C(board.SCL, board.SDA)
		self.sensor = adafruit_fxos8700.FXOS8700(i2c)
		self.heading = 0.0

		if calibrate == 1:
			self.calibrate()



