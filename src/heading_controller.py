from heading_tools import *

def heading_controller(heading_demand, imu, motor_control):
	heading_acceptence = 3
	base_speed = 23
	max_speed = 75

	heading = imu.get_heading()

	error = get_heading_difference(heading_demand, heading)

	if abs(error) > heading_acceptence:
		motor_control.stop()
		print('Making heading adjustments...')

	while abs(error) > heading_acceptence:

		heading = imu.get_heading()
		error = get_heading_difference(heading_demand, heading)

		speed = base_speed + ((abs(error)/180) * (max_speed - base_speed))

		if error < 0:
			print('Heading = {} , Demand = {} , Error = {}, Turning Left , Speed = {}'.format(heading, heading_demand, error, speed))
			motor_control.turn_left(speed)

		elif error > 0:
			print('Heading = {} , Demand = {} , Error = {}, Turning Right , Speed = {}'.format(heading, heading_demand, error, speed))
			motor_control.turn_right(speed)

	print('Heading = {} , Demand = {} , Error = {}, Stopping , Speed = {}'.format(heading, heading_demand, error, speed))
	motor_control.stop()