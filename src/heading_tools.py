
def get_heading_difference(h1, h2):
	difference = abs(h1 - h2)

	if difference > 180:
		if h1 > h2:
			difference = -1 * (h2 + (360 - h1))
		else:
			difference = h1 + (360 - h2)

	else: 
		if h1 > h2:
			difference = h1 - h2

		else:
			difference = -1 * abs(h1 - h2)

	return difference 