import time
import board
import adafruit_hcsr04
import numpy as np


class distance_sensor_class():
    def get_distance(self):
        dist = 0
        for i in range(100):
            dist += self.sensor.distance

        self.distance = dist / 100.0

        return self.distance


    def __init__(self, trigger_pin = board.D13, echo_pin = board.D19):
        self.sensor = adafruit_hcsr04.HCSR04(trigger_pin, echo_pin)