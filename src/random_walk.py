from imu_class import adafruit_imu
from motor_class import motor_control_class
from random import random
from heading_controller import heading_controller
from distance_sensor_class import distance_sensor_class

imu = adafruit_imu()
motor_control = motor_control_class()
distance_sensor = distance_sensor_class()

while True:

	heading_demand = round(random() * 360)
	print('New heading {}'.format(heading_demand))

	while distance_sensor.get_distance() > 20:
	##################################################
	################ Heading control #################
	##################################################

		heading_controller(heading_demand, imu, motor_control)

	##################################################
	#################### Movement ####################
	##################################################

		print('Moving forward')

		motor_control.forward(75)






