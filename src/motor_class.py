import RPi.GPIO as GPIO          
from time import sleep

class motor_control_class():

	def backward(self, speed):
		self.set_speed(speed)
		GPIO.output(self.in1,GPIO.HIGH)
		GPIO.output(self.in2,GPIO.LOW)
		GPIO.output(self.in3,GPIO.HIGH)
		GPIO.output(self.in4,GPIO.LOW)

	def forward(self, speed):
		self.set_speed(100 - speed)
		GPIO.output(self.in1,GPIO.LOW)
		GPIO.output(self.in2,GPIO.HIGH)
		GPIO.output(self.in3,GPIO.LOW)
		GPIO.output(self.in4,GPIO.HIGH)

	def turn_right(self, speed):
		self.pr.ChangeDutyCycle(speed)
		self.pl.ChangeDutyCycle(100-speed)

		GPIO.output(self.in1,GPIO.HIGH)
		GPIO.output(self.in2,GPIO.LOW)
		GPIO.output(self.in3,GPIO.LOW)
		GPIO.output(self.in4,GPIO.HIGH)

	def turn_left(self, speed):
		self.pr.ChangeDutyCycle(100-speed)
		self.pl.ChangeDutyCycle(speed)
		
		GPIO.output(self.in1,GPIO.LOW)
		GPIO.output(self.in2,GPIO.HIGH)
		GPIO.output(self.in3,GPIO.HIGH)
		GPIO.output(self.in4,GPIO.LOW)

	def stop(self):
		self.set_speed(0)
		GPIO.output(self.in1,GPIO.LOW)
		GPIO.output(self.in2,GPIO.LOW)
		GPIO.output(self.in3,GPIO.LOW)
		GPIO.output(self.in4,GPIO.LOW)

	def set_speed(self, speed):
		self.pr.ChangeDutyCycle(speed)
		self.pl.ChangeDutyCycle(speed)

	def set_rspeed(self, speed):
		self.pr.ChangeDutyCycle(speed)

	def set_lspeed(self, speed):
		self.pl.ChangeDutyCycle(speed)

	def __init__(self):
		GPIO.setwarnings(False)
		GPIO.cleanup()

		self.in1 = 6
		self.in2 = 5
		self.in3 = 9
		self.in4 = 10

		GPIO.setmode(GPIO.BCM)

		GPIO.setup(self.in1,GPIO.OUT)
		GPIO.setup(self.in2,GPIO.OUT)
		GPIO.output(self.in1,GPIO.LOW)
		GPIO.output(self.in2,GPIO.LOW)

		GPIO.setup(self.in3,GPIO.OUT)
		GPIO.setup(self.in4,GPIO.OUT)
		GPIO.output(self.in3,GPIO.LOW)
		GPIO.output(self.in4,GPIO.LOW)

		self.pr=GPIO.PWM(self.in1,1000)
		self.pl=GPIO.PWM(self.in3,1000)

		self.pr.start(0)
		self.pl.start(0)

#motor = motor_control_class()
#for speed in [30, 25, 23, 20]:
#	print(speed)
#	motor.turn_right(speed)
#	sleep(5)
#	motor.stop()
#	sleep(1)
#motor.stop()


